#include <SoftwareSerial.h>

SoftwareSerial BT(10,11); //rx + tx

String cmd;

void setup() {
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  BT.begin(9600);
  Serial.begin(9600);
  digitalWrite(3,LOW);
  digitalWrite(4,LOW);
}

void loop() {
 
  if(BT.available()>0)
  
  {
    cmd=BT.readString();
    
    if(cmd=="turn on the light")
    {
        digitalWrite(3, HIGH); 
        //digitalWrite(4,LOW); 
       // Serial.print("LIGHT ON");    
    }

    else if(cmd == "turn off the light")
    {
      digitalWrite(3,LOW);
      //digitalWrite(4,LOW);
      
      }
    else if(cmd=="turn on the charger")
    {
      //digitalWrite(3, LOW);
      digitalWrite(4, HIGH);
      Serial.print("OFF");
    }
    else if(cmd == "turn off the charger")
    {
      //digitalWrite(3,LOW);
      digitalWrite(4,LOW);
    }
  }
}
